from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from .forms import NewUserForm, ActivityForm, ParagraphErrorList
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages #import messages
from django.contrib.auth.forms import AuthenticationForm
from .models import Activities, Tags, Records
from datetime import date, datetime, time
from django.db.models import Q
from django.http import JsonResponse

def register_request(request):
    if not request.user.is_authenticated:
        if request.method == "POST":
            form = NewUserForm(request.POST)
            if form.is_valid():
                user = form.save()
                login(request, user)
                messages.success(request, "Registration successful." )
                return redirect("activity_register")
            messages.error(request, "Unsuccessful registration. Invalid information.")
        form = NewUserForm
        return render(request, "app/register.html" , {"register_form":form})
    else:
        return redirect("activity_register")

def login_request(request):
    if not request.user.is_authenticated:
        if request.method == "POST":
            form = AuthenticationForm(request, data=request.POST)
            if form.is_valid():
                username = form.cleaned_data.get('username')
                password = form.cleaned_data.get('password')
                user = authenticate(username=username, password=password)
                if user is not None:
                    login(request, user)
                    messages.info(request, f"You are now logged in as {username}.")
                    return redirect("activity_register")
                else:
                    messages.error(request,"Invalid username or password.")
            else:
                messages.error(request,"Invalid username or password.")
        form = AuthenticationForm()
        return render(request=request, template_name="app/login.html", context={"login_form":form})
    else:
        return redirect("activity_register")

def disconnect_request(request):
    if request.user.is_authenticated:
        logout(request)
        messages.info(request, "You are now disconnected")
    form = AuthenticationForm()
    context = {
        "login_form":form,
    }
    return render(request, "app/login.html", context)

def tags_list(request):
    if request.user.is_authenticated:
        tags = Tags.objects.all()
        context = {
            "tags":tags,
        }
        return render(request, "app/tags_list.html", context)
    else:
        return redirect("login")

def tags_delete(request, id, path_page):
    if request.user.is_authenticated:
        if Tags.objects.filter(pk=id):
            Tags.objects.get(pk=id).delete()
        return redirect(path_page)
    else:
        messages.error(request, "You must be logged")
        return redirect("login")

def activity_list_ON(request = None):
    records_ON = Records.objects.filter(end_date = None).order_by("-end_date")
    activities_ON_dict = list()
    context = {}
    for record in records_ON:
        activity_dict = dict()
        activity_dict["id"] = record.id
        activity_dict["name"] = record.activities.name
        activity_dict["start_date"] = record.start_date
        activity_dict["end_date"] = record.end_date
        activity_dict["notes"] = record.notes
        activity_dict["tag"] = ";".join([tag.name for tag in record.activities.tags.all()])
        activities_ON_dict.append(activity_dict)
        context = {"activities_ON":activities_ON_dict}
    if request:
        return render(request, "app/activity.html", context)
    else:
        return activities_ON_dict

def activity_register(request, id=None):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = ActivityForm(request.POST, user=request.user, error_class=ParagraphErrorList)
            if form.is_valid():
                # ACTIVITIES
                # If this activity already exist
                if Activities.objects.filter(name = form.cleaned_data.get("name")):
                    activity = Activities.objects.get(name = form.cleaned_data.get("name"))
                else:
                    activity = Activities.objects.create(
                        user = request.user,
                        name = form.cleaned_data.get('name'))
                    activity.save()

                # RECORDS
                # If modification
                if form.cleaned_data.get("pk"):
                    record = Records.objects.get(pk=form.cleaned_data.get("pk"))
                    record.start_date = form.cleaned_data.get("start_date")
                    record.end_date = form.cleaned_data.get("end_date")
                    record.activities = activity
                    record.notes = form.cleaned_data.get("notes")
                else:
                    record = Records.objects.create(
                        user = request.user,
                        start_date = form.cleaned_data.get("start_date"),
                        end_date =  form.cleaned_data.get("end_date"),
                        activities = activity,
                        notes = form.cleaned_data.get("notes"))
                record.save()

                # TAG'S CREATION
                # Parse tags input
                tags = form.cleaned_data.get("tag").split("; ")
                if tags[-1][-1] == ";" or tags[-1][-1] == " ":
                    tags[-1] = tags[-1][:-1]

                # Create unexisting tag and link them to activity
                for tag_name in tags:
                    if not Tags.objects.filter(name=tag_name).exists():
                        tag = Tags.objects.create(name=tag_name, user=request.user)
                        tag.save()
                        messages.info(request, f"{tag.name} has been saved.")
                    else:
                        tag = Tags.objects.get(name=tag_name)
                    if tag not in activity.tags.all():
                        activity.tags.add(tag)

                # Remove tags which are no more related to activity
                for tag_activity in activity.tags.all():
                    if tag_activity.name not in tags:
                        activity.tags.remove(tag_activity)

                activity.save()
            else:
                messages.error(request, "Error while saving this activity.")
        if id:
            record = Records.objects.get(pk=id)
            tags = record.activities.tags.all()
            tags_name = [tag.name for tag in tags]
            form = ActivityForm(user = request.user, initial={"pk":id,
                                                              "name":record.activities.name,
                                                              "tag":"; ".join(tags_name),
                                                              "start_date": record.start_date,
                                                              "end_date":record.end_date,
                                                              "notes": record.notes})
        else:
            form = ActivityForm(user=request.user)
        context = {
            "activities_ON":activity_list_ON(),
            "activity_form":form,
            "favorites_activities":Activities.objects.filter(favorite=True)}
        return render(request, "app/activity.html", context)
    else:
        messages.error(request, "You must be logged")
        return redirect("login")

def repeat_activity(request, id, path_page, is_record):
    if request.user.is_authenticated:
        if is_record == "True":
            if Records.objects.get(pk=id):
                record = Records.objects.get(pk=id)
                repeated_record = Records.objects.create(
                    user = request.user,
                    activities = record.activities,
                    start_date = datetime.today()
                )
                repeated_record.save()
        else:
            if Activities.objects.get(pk=id):
                activity = Activities.objects.get(pk=id)
                record = Records.objects.create(
                    user = request.user,
                    activities = activity,
                    start_date = datetime.today()
                )
                record.save()
        return redirect(path_page)
    else:
        messages.error(request, "You must be logged")
        return redirect("login")


def delete_activity(request, id, path_page):
    if request.user.is_authenticated:
        if Records.objects.get(pk=id):
            Records.objects.get(pk=id).delete()
            messages.error(request, "Record has been correctly removed")
        return redirect(path_page)
    else:
        messages.error(request, "You must be logged")
        return redirect("login")

def activity_list_OFF(request):
    today_min = datetime.combine(date.today(), time.min)
    records_OFF_today = Records.objects.filter(~Q(end_date = None) & Q(start_date__gt = today_min)).order_by("-end_date")
    records_OFF_all = Records.objects.filter(~Q(end_date = None) & Q(start_date__lte = today_min)).order_by("-end_date")
    activities_OFF_dict_today = list()
    activities_OFF_dict_all = list()
    for record in records_OFF_all:
        activity_dict_all = dict()
        activity_dict_all["id"] = record.id
        activity_dict_all["name"] = record.activities.name
        activity_dict_all["start_date"] = record.start_date
        activity_dict_all["end_date"] = record.end_date
        activity_dict_all["notes"] = record.notes
        activity_dict_all["tag"] = ";".join([tag.name for tag in record.activities.tags.all()])
        activities_OFF_dict_all.append(activity_dict_all)
    for record in records_OFF_today:
        activity_dict_today = dict()
        activity_dict_today["id"] = record.id
        activity_dict_today["name"] = record.activities.name
        activity_dict_today["start_date"] = record.start_date
        activity_dict_today["end_date"] = record.end_date
        activity_dict_today["notes"] = record.notes
        activity_dict_today["tag"] = ";".join([tag.name for tag in record.activities.tags.all()])
        activities_OFF_dict_today.append(activity_dict_today)
    context = {
        "activities_OFF_all":activities_OFF_dict_all,
        "activities_OFF_today":activities_OFF_dict_today,
    }
    return render(request, "app/activity_list_OFF.html", context)

def set_favorite_activity(request, id, path_page):
    if request.user.is_authenticated:
        record = Records.objects.get(pk=id)
        activity = record.activities
        if activity.favorite is False:
            activity.favorite = True
        else:
            activity.favorite = False
        activity.save()
        return redirect(path_page)
    else:
        messages.error(request, "You must be logged")
        return redirect("login")

def stop_activity(request, id, path_page):
    if request.user.is_authenticated:
        record = Records.objects.get(pk=id)
        record.end_date = datetime.now()
        record.save()
        return redirect(path_page)
    else:
        messages.error(request, "You must be logged")
        return redirect("login")

def delete_record(request, id):
    if request.user.is_authenticated:
        record = Records.objects.get(pk=id)
        for record in activity.records.filter(end_date__isnull=True):
            record.delete()
        return redirect("activity_list_ON")
    else:
        messages.error(request, "You must be logged")
        return redirect("login")

@csrf_exempt
def autocomplete(request):
    tag_query = request.GET.get('tag_query', '')
    if ("; " in tag_query):
        tag_query = tag_query.split("; ")
        tag_query = tag_query[-1]
    print(tag_query)
    tags = Tags.objects.filter(name__icontains=tag_query).values_list("name", flat=True)
    tags = list(tags)
    data = {
        'tags': tags,
    }
    return JsonResponse(data)

