function split( val ) {
    return val.split( /;\s*/ );
}

function extractLast( term ) {
    return split( term ).pop();
}

jQuery(function() {
    $("#id_tag").on('keyup', function(){
        let value = $(this).val();
        $.ajax({
            url: ajax_autocomplete_url,
            data: {
                'tag_query': value 
            },
            dataType: 'json',
            success: function (data) {
                let tags = data.tags;
                console.log(tags)
                $("#id_tag").autocomplete({
                    minChars: 0,
                    minLength: 0,
                    source: function( request, response ) {
                        // delegate back to autocomplete, but extract the last term
                        response( $.ui.autocomplete.filter(
                            tags, extractLast( request.term ) ) );
                    },
                    focus: function() {
                        // prevent value inserted on focus
                        return false;
                    },
                    select: function( event, ui ) {
                        let terms = split( this.value );
                        terms.pop();
                        terms.push( ui.item.value );
                        terms.push( "" );
                        this.value = terms.join( "; " );
                        return false;
                    }
                });       
            }
        });        
    });
});
