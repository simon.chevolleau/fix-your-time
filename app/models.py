from django.db import models
from fix_your_time import settings
from django.contrib.auth.models import User
from datetime import datetime

class Tags(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=500, unique = True)

class Activities(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=500)
    favorite = models.BooleanField(default = False)
    tags = models.ManyToManyField(Tags)

class Records(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    start_date = models.DateTimeField(default=datetime.now)
    end_date = models.DateTimeField(null = True)
    activities = models.ForeignKey(Activities, on_delete=models.CASCADE)
    notes = models.CharField(max_length=5000, default = "")
