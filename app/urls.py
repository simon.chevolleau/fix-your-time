from django.urls import path
from . import views

urlpatterns = [
    path("activity", views.activity_register, name="activity_register"),
    path("activity_register/<int:id>", views.activity_register, name="activity_register"),
    path("activity/<int:id>", views.activity_register, name="activity_register"),
    path('register', views.register_request, name="register"),
    path("login", views.login_request, name="login"),
    path("disconnect", views.disconnect_request, name="disconnect"),
    path('ajax/autocomplete', views.autocomplete, name='ajax/autocomplete'),
    path('activity_list_ON', views.activity_list_ON, name="activity_list_ON"),
    path('activity_list_OFF', views.activity_list_OFF, name="activity_list_OFF"),
    path('delete_activity/<int:id>/<path:path_page>', views.delete_activity, name="delete_activity"),
    path('repeat_activity/<int:id>/<path:path_page>/<str:is_record>', views.repeat_activity, name="repeat_activity"),
    path('set_favorite_activity/<int:id>/<path:path_page>', views.set_favorite_activity, name="set_favorite_activity"),
    path('stop_activity/<int:id>/<path:path_page>', views.stop_activity, name="stop_activity"),
    path('tags_list', views.tags_list, name="tags_list"),
    path('tags_delete/<int:id>/<path:path_page>', views.tags_delete, name="tags_delete"),
    # path("record", views.viewGame, name="viewGame"),
    # path('list_records', views.list_records, name="list_records"),
    # path('list_categories', views.list_categories, name="list_categories"),
]
