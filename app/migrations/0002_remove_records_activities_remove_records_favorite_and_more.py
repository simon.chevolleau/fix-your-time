# Generated by Django 4.0 on 2022-03-29 19:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='records',
            name='activities',
        ),
        migrations.RemoveField(
            model_name='records',
            name='favorite',
        ),
        migrations.AddField(
            model_name='activities',
            name='favorite',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='activities',
            name='records',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='app.records'),
            preserve_default=False,
        ),
    ]
