from django.forms import Form, ModelForm, EmailField, CharField, DateTimeField, ModelChoiceField, Select, IntegerField, Textarea
from django.forms.widgets import DateTimeInput
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms.utils import ErrorList
from datetime import datetime

class ParagraphErrorList(ErrorList):
	def __str__(self):
		return self.as_divs()
	def as_divs(self):
		if not self: return ''
		return '<div">%s</div>' % ''.join(['<p">%s</p>' % e for e in self])

class NewUserForm(UserCreationForm):
	email = EmailField(required=True)
	class Meta:
		model = User
		fields = ("username", "email", "password1", "password2")

	def save(self, commit=True):
		user = super(NewUserForm, self).save(commit=False)
		user.email = self.cleaned_data['email']
		if commit:
			user.save()
		return user

class ActivityForm(Form):
    pk = IntegerField(label="pk", required=False)
    name = CharField(label="name", required=True)
    tag = CharField(label="tag", required=True)
    start_date = DateTimeField(initial=datetime.now, label="start_date", required=False)
    end_date = DateTimeField(label="end_date", required=False)
    notes = CharField(label="notes", required=False, widget=Textarea())
    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(ActivityForm, self).__init__(*args, **kwargs)
