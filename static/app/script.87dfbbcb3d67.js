// jQuery(function() {
//     $("#id_tag").on('keydown', function(){
//         let value = $(this).val();
//         $.ajax({
//             url: "{% url 'ajax_autocomplete' %}",
//             data: {
//               'tag_query': value 
//             },
//             dataType: 'json',
//             success: function (data) {
//                 let tags = data.tags;
//                 $("#id_tag").autocomplete({
//                 source: tags,
//                 minLength: 3 
//                 });       
//             }
//         });        
//     });
//   });

function split( val ) {
    return val.split( /;\s*/ );
}

function extractLast( term ) {
    return split( term ).pop();
}

// jQuery(function() {
//     $('#id_tag').on("keydown", function(){
//         let value = $(this).val();
//         $.getJSON({
//             url: "{% url 'ajax_autocomplete' %}",
//             data: {
//               'tag_query': value 
//             },
//             success: function (data) {
//                 let tags = data.tags;
//                 $("#id_tag").autocomplete({
//                     source: function(request, response) [
//                     reponse($.ui.autocomplete.filter())
//                     ],
//                     minLength: 2 
//                 });       
//             }
//         })
//     })
// })

// jQuery(function() {
//     $("#id_tag")
//         .on( "keypress", function( event ) {
//             if ( event.keyCode === $.ui.keyCode.TAB &&
//                 $( this ).autocomplete( "instance" ).menu.active ) {
//                 event.preventDefault();
//             }
//         })
//         .autocomplete({
//         minChars: 1,
//         delimiter: '; ',
//         focus: fonction() { return false; },
//         source: "{% url 'ajax_autocomplete' %}",
//         response: function(event, ui) {
//             if (!ui.content.length) {
//                 var noResult = { value: "", label: "No data found" };
//                 ui.content.push(noResult);
//             }
//         },
//         select: function (e, ui) {
//             if (ui.item.value) {
//                 var terms = split( this.value );
//                 // remove the current input
//                 terms.pop();
//                 // add the selected item
//                 terms.push( ui.item.value );
//                 // add placeholder to get the comma-and-space at the end
//                 terms.push( "" );
//                 this.value = terms.join( "; " );
//                 return false;
//             }
//         }
//     });
// })

$( function() {
    var availableTags = [
      "ActionScript",
      "AppleScript",
      "Asp",
      "BASIC",
      "C",
      "C++",
      "Clojure",
      "COBOL",
      "ColdFusion",
      "Erlang",
      "Fortran",
      "Groovy",
      "Haskell",
      "Java",
      "JavaScript",
      "Lisp",
      "Perl",
      "PHP",
      "Python",
      "Ruby",
      "Scala",
      "Scheme"
    ];
    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#id_tag" )
      // don't navigate away from the field on tab when selecting an item
      .on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            availableTags, extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      });
  } );
